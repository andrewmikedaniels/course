// Program1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>

const int NUMBER_OF_BITS = 8;

int main()
{
	int choice;
	char con = 'y';

	int max = (int)(pow(2, NUMBER_OF_BITS - 1)) - 1;
	int complement = max + 2;

	while (con == 'y')
	{
		std::cout << "Enter your choice (1 = decimal to binary, 2 = binary to decimal): ";
		std::cin >> choice;
		std::cin.ignore(1000, '\n');

		switch (choice)
		{
		case 1:
		{
			int in;
			std::string out = "";

			std::cout << "Enter the signed decimal number to be converted: ";
			std::cin >> in;
			std::cin.ignore(1000, '\n');

			int temp = in;

			if (in < 0)
			{
				temp = abs(max - abs(temp)) + complement;
			}

			while (temp != 0)
			{
				std::ostringstream oss;
				oss << temp % 2;
				out = oss.str() + out;
				temp = temp / 2;
			}

			while (out.length() < NUMBER_OF_BITS)
			{
				out = "0" + out;
			}

			std::cout << "The binary equivalent for signed decimal number " << in << " is " << out << std::endl;

			break;
		}
		case 2:
		{
			std::string in;
			int out = 0;

			std::cout << "Enter the binary number to be converted: ";
			getline(std::cin, in);

			for (int i = 0; i < in.length(); ++i)
			{
				out += (int)(in[in.length() - 1 - i] - '0') * pow(2, i);
			}

			if (out > max)
			{
				out = -(max - abs(out - complement));
			}

			std::cout << "The signed decimal equivalent for binary number " << in << " is " << out << std::endl;

			break;
		}
		}

		std::cout << "More choice? (y or n): ";
		std::cin.get(con);
		std::cin.ignore(1000, '\n');
	}

	return 0;
}
