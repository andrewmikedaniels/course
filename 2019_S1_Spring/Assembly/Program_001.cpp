// Assignment001.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>

int main()
{
	int choice;
	char con = 'y';
	
	while (con == 'y')
	{
		std::cout << "Enter your choice (1 = decimal to binary, 2 = binary to decimal): ";
		std::cin  >> choice;
		std::cin.ignore(1000, '\n');
		
		switch (choice)
		{
			case 1:
			{
				int in;
				std::string out = "";
				
				std::cout << "Enter the unsigned decimal number to be converted: ";
				std::cin  >> in;
				std::cin.ignore(1000, '\n');
				
				int temp = in;
				
				while (temp != 0)
				{
					std::ostringstream oss;
					oss << temp % 2;
					out = oss.str() + out;
					temp = temp / 2;
				}
				
				std::cout << "The binary equivalent for unsigned decimal number " << in << " is " << out << std::endl;
				
				break;
			}
			case 2:
			{
				std::string in;
				int out = 0;
				
				std::cout << "Enter the binary number to be converted: ";
				getline(std::cin, in);
				
				for (int i = 0; i < in.length(); ++i)
				{
					out += (int)(in[in.length() - 1 - i] - '0') * pow(2, i);
				}
				
				std::cout << "The unsigned decimal equivalent for binary number " << in << " is " << out << std::endl;
				
				break;
			}
		}
		
		std::cout << "More choice? (y or n): ";
		std::cin.get(con);
		std::cin.ignore(1000, '\n');
	}
	
    return 0;
}
