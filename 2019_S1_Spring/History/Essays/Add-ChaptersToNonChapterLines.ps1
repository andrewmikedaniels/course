Param
(
	[String]
	$FilePath
)

$CHAPTER_PATTERN = "^\d+"
$chapter = 0

Get-Content $FilePath | % { $capture = [Regex]::Match($_, $CHAPTER_PATTERN); if ($capture.Success) { $chapter = $capture.Value; $_ } else { $chapter + $_ } }
