// This program calls copyfile() to copy a file from standard input to standard output.

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <unistd.h>

#define BLKSIZE 10

int copyfile(int fromfd, int tofd); // the function prototype

int main (void) {
   int numbytes;
   numbytes = copyfile(STDIN_FILENO, STDOUT_FILENO);
   fprintf(stderr, "Number of bytes copied: %d\n", numbytes);
   return 0;
}

int copyfile(int fromfd, int tofd) {
   char *bp;
   char buf[BLKSIZE];
   int bytesread, byteswritten;
   int totalbytes = 0;
   int done = 0;

   while (!done) { // point a
      while (((bytesread = read(fromfd, buf, BLKSIZE)) == -1) &&
            (errno == EINTR)); // handle interruption by a signal

      if (bytesread <= 0) // real error or end-of-file on fromfd
         break; // out of the while loop at point a

      bp = buf;

      while (bytesread > 0) { // point b
         while (((byteswritten = write(tofd, bp, bytesread)) == -1 ) &&
               (errno == EINTR)); // handle interruption by a signal

         if (byteswritten <= 0) // real error on tofd
            break; // out of the while loop at point b

         totalbytes += byteswritten;
         bytesread -= byteswritten;

         if (bytesread == 0) // there is no guarantee that bytes written equal bytes read
            done = 1;
      }

      if (byteswritten == -1) // real error on tofd
         break; // out of the while loop at point a
   }

   return totalbytes;
}
