// This program shows how to read a specific number of bytes for a struct type.

#include <errno.h>
#include <unistd.h>
#include <stdio.h>

struct { // a total of 8 bytes
   int x;
   int y;
} point;

ssize_t readblock(int fd, void *buf, size_t size); // the function prototype

int main(void) {
   if (readblock(STDIN_FILENO, &point, sizeof(point)) <= 0)
      fprintf(stderr, "Cannot read a point.\n");
   return 0;
}

ssize_t readblock(int fd, void *buf, size_t size) {
   char *bufp;
   size_t bytestoread;
   ssize_t bytesread;
   size_t totalbytes;

   for (bufp = buf, bytestoread = size, totalbytes = 0; bytestoread > 0; bufp 
        += bytesread, bytestoread -= bytesread) {
      bytesread = read(fd, bufp, bytestoread); // start reading here

      fprintf(stderr, "the starting address is %d\n", bufp);

      if ((bytesread == 0) && (totalbytes == 0)) { // case 1
         fprintf(stderr, "EOF encountered before reading any bytes\n");
         return 0;
      }

      if (bytesread == 0) { // case 2
         fprintf(stderr, "EOF encountered after reading some bytes\n");
         return -1;
      }

      if (bytesread == -1 && (errno != EINTR)) { // case 3
         fprintf(stderr, "Interrupted by a signal\n");
         return -1;
      }

      if (bytesread == -1) { // case 4
         fprintf(stderr, "Real error reading bytes occurred\n");
         return -1;
      }

      totalbytes += bytesread;
      fprintf(stderr, "the ending address is %d\n", bufp + bytesread - 1);
   }

   return totalbytes;
}

