// This program uses the select() function to handle two concurrent file transfers.

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <unistd.h>

#define BLKSIZE 1024
#define READ_FLAGS O_RDONLY
#define WRITE_FLAGS (O_WRONLY | O_CREAT)
#define WRITE_PERMS (S_IRUSR | S_IWUSR)

int copyfile(int fromfd, int tofd);

int copy2file(int fromfd1, int tofd1, int fromfd2, int tofd2);

int main(int argc, char *argv[]) {
   int bytes;
   int fromfd1, tofd1;
   int fromfd2, tofd2;

   if (argc != 5) {
      fprintf(stderr, "Usage: %s from_file to_file\n", argv[0]);
      return 1;
   }

   if ((fromfd1 = open(argv[1], READ_FLAGS)) == -1) { // myFile1in.dat
      perror("Failed to open input file 1");
      return 1;
   }

   if ((tofd1 = open(argv[2], WRITE_FLAGS, WRITE_PERMS)) == -1) { // myFile1out.dat
      perror("Failed to create output file 1");
      return 1;
   }

   if ((fromfd2 = open(argv[3], READ_FLAGS)) == -1) { // myFile2in.dat
      perror("Failed to open input file 2");
      return 1;
   }

   if ((tofd2 = open(argv[4], WRITE_FLAGS, WRITE_PERMS)) == -1) { // myFile2out.dat
      perror("Failed to create output file 2");
      return 1;
   }

   bytes = copy2files(fromfd1, tofd1, fromfd2, tofd2);

   fprintf(stderr, "Total bytes read for both files are %d including 2 \\n\n", bytes);

   return 0; // the return closes the files
}

int copyfile(int fromfd, int tofd) {
   char *bp;
   char buf[BLKSIZE];
   int bytesread, byteswritten;
   int totalbytes = 0;
   int done = 0;

   while (!done) {
      while (((bytesread = read(fromfd, buf, BLKSIZE)) == -1) &&
            (errno == EINTR)); // handle interruption by signal

      if (bytesread <= 0) // real error or end-of-file on fromfd
         break;

      bp = buf;

      while (bytesread > 0) {
         while (((byteswritten = write(tofd, bp, bytesread)) == -1 ) &&
               (errno == EINTR)); // handle interruption by signal

         if (byteswritten <= 0) //  real error on tofd
            break;

         totalbytes += byteswritten;
         bytesread -= byteswritten;

         if (bytesread == 0)
            done = 1;
      }

      if (byteswritten == -1) // real error on tofd
         break;
   }

   return totalbytes;
}

int copy2files(int fromfd1, int tofd1, int fromfd2, int tofd2) {
   int bytesread;
   int maxfd;
   int num;
   fd_set readset;
   int totalbytes = 0;

   if ((fromfd1 < 0) || (fromfd1 >= FD_SETSIZE) || (tofd1 < 0) || (tofd1 >= FD_SETSIZE) ||
       (fromfd2 < 0) || (fromfd2 >= FD_SETSIZE) || (tofd2 < 0) || (tofd2 >= FD_SETSIZE))
      return 0;

   maxfd = fromfd1; // find the highest fd number for select

   if (fromfd2 > maxfd)
      maxfd = fromfd2;

   for ( ; ; ) {
      FD_ZERO(&readset);
      FD_SET(fromfd1, &readset);
      FD_SET(fromfd2, &readset);

      if (((num = select(maxfd+1, &readset, NULL, NULL, NULL)) == -1) && (errno == EINTR))
         continue;

      if (num == -1) // none of the two input file descriptors is ready
         return totalbytes; // 0 is returned

      if (FD_ISSET(fromfd1, &readset)) { // check if file descriptor 1 is ready
         bytesread = copyfile(fromfd1, tofd1);

         if (bytesread <= 0)
            break;

         totalbytes += bytesread;
      }
      if (FD_ISSET(fromfd2, &readset)) { // check if file descriptor 2 is ready
         bytesread = copyfile(fromfd2, tofd2);

         if (bytesread <= 0)
            break;

         totalbytes += bytesread;
      }
   }

   return totalbytes; // total bytes transfered for both file descriptors
}
