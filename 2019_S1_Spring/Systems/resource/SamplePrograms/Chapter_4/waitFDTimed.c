// This program waits a given time for input to be available from STDIO_FILENO.
// The program uses a function (i.e. gettimeout()) to determine timeout.

#include <errno.h>
#include <string.h>
#include <sys/select.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

#define BLKSIZE 1024
#define MILLION 1000000L // one second is 1 million microseconds
#define D_MILLION 1000000.0

int copyfile(int fromfd, int tofd);

static int gettimeout(struct timeval end, struct timeval *timeoutp);

int waitfdtimed(int fd, struct timeval end);

int main (void) {
   struct timeval timeout;
   timeout.tv_sec = 5; // seconds
   timeout.tv_usec = 0; // microseconds
   int numbytes;

   waitfdtimed(STDIN_FILENO, timeout);
   numbytes = copyfile(STDIN_FILENO, STDOUT_FILENO);
   fprintf(stderr, "Number of bytes copied: %d\n", numbytes);

   return 0;
}

int copyfile(int fromfd, int tofd) {
   char *bp;
   char buf[BLKSIZE];
   int bytesread, byteswritten;
   int totalbytes = 0;
   int done = 0;

   while (!done) {
      while (((bytesread = read(fromfd, buf, BLKSIZE)) == -1) &&
            (errno == EINTR)); // handle interruption by signal

      if (bytesread <= 0) // real error or end-of-file on fromfd
         break;

      bp = buf;

      while (bytesread > 0) {
         while (((byteswritten = write(tofd, bp, bytesread)) == -1 ) &&
               (errno == EINTR)); // handle interruption by signal

         if (byteswritten <= 0) //  real error on tofd
            break;

         totalbytes += byteswritten;
         bytesread -= byteswritten;

         if (bytesread == 0)
            done = 1;
      }

      if (byteswritten == -1) // real error on tofd
         break;
   }

   return totalbytes;
}

static int gettimeout(struct timeval end, struct timeval *timeoutp) {
   gettimeofday(timeoutp, NULL); // get time of day

   timeoutp->tv_sec = (end.tv_sec + timeoutp->tv_sec) - timeoutp->tv_sec;
   timeoutp->tv_usec = end.tv_usec - timeoutp->tv_usec;

   if (timeoutp->tv_usec >= MILLION) { // adjust from microsecond to second
      timeoutp->tv_sec++;
      timeoutp->tv_usec -= MILLION;
   }

   if (timeoutp->tv_usec < 0) { // adjust from second to microsecond
      timeoutp->tv_sec--;
      timeoutp->tv_usec += MILLION;
   }

   if ((timeoutp->tv_sec < 0) || ((timeoutp->tv_sec == 0) && (timeoutp->tv_usec == 0))) {
      errno = ETIME;
      return -1;
   }

   return 0;
}

int waitfdtimed(int fd, struct timeval end) {
   fd_set readset;
   int retval;
   struct timeval timeout;

   if ((fd < 0) || (fd >= FD_SETSIZE)) {
      errno = EINVAL;
      return -1;
   }

   FD_ZERO(&readset);
   FD_SET(fd, &readset);

   if (gettimeout(end, &timeout) == -1)
      return -1;

   while (((retval = select(fd + 1, &readset, NULL, NULL, &timeout)) == 1) && (errno == EINTR)) {
      if (gettimeout(end, &timeout) == -1)
         return -1;
      FD_ZERO(&readset);
      FD_SET(fd, &readset);
   }

   if (retval == 0) { // the stdin was not ready in timeout
      fprintf(stderr, "timer expired and fd 0 is returned\n");
      errno = ETIME;
      return -1;
   }

   if (retval == -1) // error occurred 
      return -1;

   return 0;
}
