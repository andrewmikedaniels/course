// This program blocks until one of the file descriptors is ready.
// The program waits until either input file (for READ) is ready.
// FD_SETSIZE is a macro returning the maximum number of file descriptors for a fd_set.

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <fcntl.h>

int whichisready(int fromfd, int tofd);

int main(int argc, char *argv[]) {
   int bytesread;
   int childpid;
   int fd, fd1, fd2;

   if (argc != 3) {
      fprintf(stderr, "Usage: %s file1 file2\n", argv[0]);
      return 1;
   }

   if ((fd1 = open(argv[1], O_RDONLY)) == -1) { // input file = myFile1.dat
      fprintf(stderr, "Failed to open file %s:%s\n", argv[1], strerror(errno));
      return 1;
   }

   if ((fd2 = open(argv[2], O_RDONLY)) == -1) { // input file = myFile2.dat
      fprintf(stderr, "Failed to open file %s:%s\n", argv[2], strerror(errno));
      return 1;
   }

   if (whichisready(fd1, fd2) == -1)
      fprintf(stderr, "The function failed!\n");
   else
      fprintf(stderr, "Monitoring is done successfully!\n");

   return 0;
}

int whichisready(int fd1, int fd2) {
   int maxfd;
   int nfds;
   fd_set readset;

   if ((fd1 < 0) || (fd1 >= FD_SETSIZE) || (fd2 < 0) || (fd2 >= FD_SETSIZE)) {
      errno = EINVAL;
      return -1;
   }

   maxfd = (fd1 > fd2) ? fd1 : fd2; // This is to determine the highest fd number

   FD_ZERO(&readset);
   FD_SET(fd1, &readset);
   FD_SET(fd2, &readset);

   nfds = select(maxfd+1, &readset, NULL, NULL, NULL);

   if (nfds == -1)
      return -1;

   if (FD_ISSET(fd1, &readset)) {
      fprintf(stderr, "fd1 is ready\n");
      // return fd1;
   }

   if (FD_ISSET(fd2, &readset)) {
      fprintf(stderr, "fd2 is ready\n");
      // return fd2;
   }

   return 0;
}
