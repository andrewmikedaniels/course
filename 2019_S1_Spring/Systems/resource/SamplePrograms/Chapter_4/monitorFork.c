// This program monitors two files by forking a child process. Output goes to stdout.
// The parent first opens two files. It then monitors 1 file and the child monitors the other.
// The parent process will read an input file and print the contents to the screen.
// The child process will read a different input file and print the contents to the screen.

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BLKSIZE 1024

int copyfile(int fromfd, int tofd); // the function prototype

int main(int argc, char *argv[]) {
   int bytesread;
   int childpid;
   int fd, fd1, fd2;

   if (argc != 3) {
      fprintf(stderr, "Usage: %s file1 file2\n", argv[0]);
      return 1;
   }

   if ((fd1 = open(argv[1], O_RDONLY)) == -1) { // input file = myFile1in.dat
      fprintf(stderr, "Failed to open file %s:%s\n", argv[1], strerror(errno));
      return 1;
   }

   if ((fd2 = open(argv[2], O_RDONLY)) == -1) { // input file = myFile2in.dat
      fprintf(stderr, "Failed to open file %s:%s\n", argv[2], strerror(errno));
      return 1;
   }

   if ((childpid = fork()) == -1) {
      perror("Failed to create child process");
      return 1;
   }

   if (childpid > 0) // parent code
      fd = fd1;
   else
      fd = fd2; // child code

   bytesread = copyfile(fd, STDOUT_FILENO); // both processes are doing this

   fprintf(stderr, "Bytes read: %d\n", bytesread);

   return 0;
}
int copyfile(int fromfd, int tofd) {
   char *bp;
   char buf[BLKSIZE];
   int bytesread, byteswritten;
   int totalbytes = 0;

   for ( ; ; ) {
      while (((bytesread = read(fromfd, buf, BLKSIZE)) == -1) &&
            (errno == EINTR)); // handle interruption by signal

      if (bytesread <= 0) // real error or end-of-file on fromfd
         break; // out of the "for" loop

      bp = buf;

      while (bytesread > 0) {
         while (((byteswritten = write(tofd, bp, bytesread)) == -1 ) &&
               (errno == EINTR)); // handle interruption by signal

         if (byteswritten <= 0) // real error on tofd
            break; // out of the outer "while" loop

         totalbytes += byteswritten;
         bytesread -= byteswritten;
         bp += byteswritten;
      }

      if (byteswritten == -1) // real error on tofd
         break; // out of the "for" loop
   }

   return totalbytes;
}
