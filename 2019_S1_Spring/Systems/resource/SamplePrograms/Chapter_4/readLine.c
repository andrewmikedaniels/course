// This program reads a line up to 10 characters (including new line character) from stdin.

#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int readline(int fd, char *buf, int nbytes); // the function prototype

int bytesread;
char mybuf[10]; // a buffer

int main() {
   fprintf(stderr, "Enter a sentence: ");
   // printf() is no good because it is a buffered output stream
   bytesread = readline(STDIN_FILENO, mybuf, sizeof(mybuf));
   // STDIN_FILENO is a preprocessor symbol for stdin
   return 0;
}

int readline(int fd, char *buf, int nbytes) {
   int numread = 0;
   int returnval;

   while (numread < nbytes - 1) { // continue to read until up to 9 bytes are read (i.e. 0..8)
      returnval = read(fd, buf + numread, 1); // read 1 byte at a time starting from buf

      if ((returnval == -1) && (errno == EINTR)){ // case 1
         fprintf(stderr, "Interrupted by a signal\n");
         continue;
      }

      if ((returnval == 0) && (numread == 0)) { // case 2
         fprintf(stderr, "Ctrl-D or EOF\n");
         return 0;
      }

      if (returnval == 0) { // case 3
         fprintf(stderr, "Not possible for stdin because it needs at least 1 character\n");
         break;
      }

      if (returnval == -1) { // case 4
         fprintf(stderr, "Real error\n");
         return -1;
      }

      fprintf(stderr, "One byte read\n"); // case 5 

      numread++;

      // let's handle the NULL terminator
      // the fragment will not be executed if more than 10 characters are entered
      if (buf[numread-1] == '\n') { // a new line character
         buf[numread] = '\0'; // NULL terminator inserted
         fprintf(stderr, "number of bytes read = %d\n", numread);
         return numread;
      }
   }

   errno = EINVAL; // for case 3

   return -1;
}


