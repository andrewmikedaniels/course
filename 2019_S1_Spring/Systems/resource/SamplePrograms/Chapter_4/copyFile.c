// This program copies from a file to another file via command-line arguments.

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

#define BLKSIZE 100
#define READ_FLAGS O_RDONLY
// #define WRITE_FLAGS (O_WRONLY | O_CREAT | O_EXCL) // This will result in open failure
#define WRITE_FLAGS (O_WRONLY | O_CREAT) // O_CREAT has no effect
#define WRITE_PERMS (S_IRUSR | S_IWUSR)

int copyfile(int fromfd, int tofd);

int main(int argc, char *argv[]) {
   int bytes;
   int fromfd, tofd; // input file == myFile1.dat, output file == myFile2.dat

   if (argc != 3) {
      fprintf(stderr, "Usage: %s from_file to_file\n", argv[0]);
      return 1;
   }

   if ((fromfd = open(argv[1], READ_FLAGS)) == -1) { // open input file
      perror("Failed to open input file");
      return 1;
   }

   if ((tofd = open(argv[2], WRITE_FLAGS, WRITE_PERMS)) == -1) { // open output file
      perror("Failed to create output file");
      return 1;
   }

   bytes = copyfile(fromfd, tofd);

   printf("%d bytes copied from %s to %s\n", bytes, argv[1], argv[2]);

   return 0; // the return function closes the files
}
int copyfile(int fromfd, int tofd) {
   char *bp;
   char buf[BLKSIZE];
   int bytesread, byteswritten;
   int totalbytes = 0;
   int done = 0;

   while (!done) {
      while (((bytesread = read(fromfd, buf, BLKSIZE)) == -1) &&
            (errno == EINTR)); // handle interruption by signal

      if (bytesread <= 0) // real error or end-of-file on fromfd
         break;

      bp = buf;

      while (bytesread > 0) {
         while (((byteswritten = write(tofd, bp, bytesread)) == -1 ) &&
               (errno == EINTR)); // handle interruption by signal

         if (byteswritten <= 0) //  real error on tofd
            break;

         totalbytes += byteswritten;
         bytesread -= byteswritten;

         if (bytesread == 0)
            done = 1;
      }

      if (byteswritten == -1) // real error on tofd
         break;
   }

   return totalbytes;
}
