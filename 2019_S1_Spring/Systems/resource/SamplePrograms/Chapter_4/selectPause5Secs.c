// This program waits up to 5 seconds for input from stdin to demonstrate the use of select().

#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define TIMEOUT 5 // select timeout in seconds
#define BUF_LEN 1024 // read buffer in bytes

int main(void) {
   struct timeval tv;
   fd_set readfds;
   int ret;

   // Wait on stdin for input
   FD_ZERO(&readfds); // clear all the bits in readfds
   FD_SET(STDIN_FILENO, &readfds); // file descriptor 0

   // Wait up to 5 seconds
   tv.tv_sec = TIMEOUT; // seconds
   tv.tv_usec = 0; // microseconds

   // Now block
   ret = select(STDIN_FILENO + 1, &readfds, NULL, NULL, &tv);

   if (ret == -1) {
      perror("select");
      return 1;
   }
   else if (!ret) { // no file descriptor is ready (i.e. ret == 0)
      printf("%d seconds elapsed\n", TIMEOUT);
      return 0;
   }

   // The file descriptor must be ready to read
   if (FD_ISSET(STDIN_FILENO, &readfds)) {
      char buf[BUF_LEN + 1];
      int len;

      // guaranteed to not block
      len = read(STDIN_FILENO, buf, BUF_LEN);
      if (len == -1) {
         perror("read");
         return 1;
      }

      if (len) {
         buf[len] = '\0';
         printf("read: %s\n", buf);
      }
      return 0;
   }

   fprintf(stderr, "This should not happen\n");

   return 1;
}
