// This program shows how to block signals while creating two pipes.

#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>

#define R_MODE (S_IRUSR | S_IRGRP | S_IROTH) // read permissions
#define W_MODE (S_IWUSR | S_IWGRP | S_IWOTH) // write permissions
#define RW_MODE (R_MODE | W_MODE) // read-write permissions

int makeTwoPipes(char *pipe1, char *pipe2); // the prototype of the function

int main(void) {
   char pipe1[] = "myPipe1";
   char pipe2[] = "myPipe2";

   printf("ready to call makeTwoPipes()\n");

   makeTwoPipes(pipe1, pipe2);

   printf("back from makeTwoPipes()\n");

   return 0;
}

int makeTwoPipes(char *pipe1, char *pipe2) {
   sigset_t blockmask; // a signal mask
   sigset_t oldmask;
   int returncode = 0;

   if (sigfillset(&blockmask) == -1) // all signals are blocked
      return -1;

   // the set of blocked signals is set to blockmask
   // the previous value of the signal mask is stored in oldmask
   if (sigprocmask(SIG_SETMASK, &blockmask, &oldmask) == -1)
      return -1;
   else
      printf("all signals are blocked\n");

   if (((mkfifo(pipe1, RW_MODE) == -1) && (errno != EEXIST)) ||
      ((mkfifo(pipe2, RW_MODE) == -1) && (errno != EEXIST))) {
      returncode = errno;
      unlink(pipe1);
      unlink(pipe2);
   }

   sleep(5); // no way to terminate the program via SIGINT

   // the set of blocked signals is set back to the previuos value
   // the program is terminated after the signal mask is set back to the original (empty) one
   if ((sigprocmask(SIG_SETMASK, &oldmask, NULL) == -1) && !returncode)
      returncode = errno;
   else
      printf("all signals are unblocked\n");

   if (returncode) {
      errno = returncode;
      return -1;
   }

   return 0;
}
