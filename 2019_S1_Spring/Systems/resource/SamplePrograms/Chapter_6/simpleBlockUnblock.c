// This program shows how to block and unblock the SIGINT signal.

#include <math.h> // add -lm (i.e. library math for sin()) to the command line for compilation
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) { // arg[1] contains # of iterations for the "for" loop
   int i;
   int j;
   sigset_t intmask;
   int repeatfactor;
   double y = 0.0;

   if (argc != 2) {
      fprintf(stderr, "Usage: %s repeatfactor\n", argv[0]);
      return 1;
   }

   repeatfactor = atoi(argv[1]);

   if ((sigemptyset(&intmask) == -1) || (sigaddset(&intmask, SIGINT) ==	 -1)){
      perror("Failed to initialize the signal mask");
      return 1;
   }

   for (j = 0; j < 3; j++) {
      // If Ctrl-C is pressed after SIGINT is blocked and before SIGINT is unblocked,
      // then it has no effect until SIGINT is unbloced and the program terminates.
      // This is the case for 100000000 below
      if (sigprocmask(SIG_BLOCK, &intmask, NULL) == -1) // SIGINT is blocked
         break;

      fprintf(stderr, "SIGINT signal blocked\n");

      for (i = 0; i < repeatfactor; i++) // try 1000 and 100000000
         y += sin((double)i);

      fprintf(stderr, "Blocked calculation is finished, y = %f\n", y);

      // sleep(5);

      if (sigprocmask(SIG_UNBLOCK, &intmask, NULL) == -1) // SIGINT is unblocked
         break;

      fprintf(stderr, "SIGINT signal unblocked\n");

      for (i = 0; i < repeatfactor; i++)
         y += sin((double)i);

      fprintf(stderr, "Unblocked calculation is finished, y= %f\n\n", y);
   }

   return 1;
}
