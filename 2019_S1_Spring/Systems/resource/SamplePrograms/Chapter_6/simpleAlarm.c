// This program shows that the default action for alarm() is to terminate the caller.
// When time specified in alarm() expires, SIGALRM is generated to cause the program to terminate.

#include <unistd.h>

int main(void) {
   alarm(5);
   for( ; ; ); // will never be executed
}
