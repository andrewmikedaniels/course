// This program checks whether or not a signal can be ignored.
// Since a signal handler is a function, it can be called directly in main().

#include <signal.h>
#include <stdio.h>
#include <unistd.h>

struct sigaction act;
int inp;
int status;

int testIgnored(int signo);

int main(void) {
   printf("Enter a signal number: ");
   scanf("%d", &inp); // enter a signal number here
   status = testIgnored(inp);

   if (status == 1)
      puts("The signal can be ignored\n");
   else
      puts("The signal cannot be ignored\n");

   return 0;
}

int testIgnored(int signo) {
   printf("signal: %s\n", sys_siglist[signo]); // signal symbolic name is displayed here

   if (sigaction(signo, NULL, &act) == -1) // retrieve signal information here
      printf("Failed to execute sigaction\n");
   else if (act.sa_handler == SIG_IGN)
      return 1; // the signal cannot be ignored
   else 
      return 0; // the signal can be ignored
}

