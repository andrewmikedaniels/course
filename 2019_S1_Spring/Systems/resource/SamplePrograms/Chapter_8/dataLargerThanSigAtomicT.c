// The program shows how user-defined signal (i.e. SIGUSR1) forces output to be printed to screen.
// main() keeps writing calculation results into the buffer via results() every 10000 iterations.
// The USR1 signal is blocked when results are written to the buffer by results().
// When USR1 signal is issued, the final results are printed to the screen by the signal handler.
 
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUFSIZE 100

static char buf[BUFSIZE]; // global variable accessible to both main() and the handler

static int buflen = 0; // global variable accessible to both main() and the handler

static void handler(int signo) { // handler outputs result string
   int savederrno;

   savederrno = errno;

   // the handler is forced by SIGUSR1 to print results from buf to screen
   write(STDOUT_FILENO, buf, buflen); // use write() instead of fprintf()
   errno = savederrno;
}

static void results(int count, double sum) { // set up result string
   double average;
   double calculated;
   double err;
   double errpercent;
   sigset_t oldset;
   sigset_t sigset;

   // block SIGUSR1 when saving results into buf to prevent SIGUSR1 from forcing the
   // results to be printed by the handler from buf to screen
   if ((sigemptyset(&sigset) == -1) || (sigaddset(&sigset, SIGUSR1) == -1) ||
      (sigprocmask(SIG_BLOCK, &sigset, &oldset) == -1) )
         perror("Failed to block signal in results");

   if (count == 0)
      snprintf(buf, BUFSIZE, "No values calculated yet\n");
   else {
      calculated = 1.0 - cos(1.0);
      average = sum/count;
      err = average - calculated;
      errpercent = 100.0*err/calculated;
      // buf is updated every time when results() is called
      snprintf(buf, BUFSIZE, "Count = %d, sum = %f, average = %f, error = %f or %f%%\n", count, 	       sum, average, err, errpercent);
   }

   buflen = strlen(buf);

   if (sigprocmask(SIG_SETMASK, &oldset, NULL) == -1) // restore previous signal set
      perror("Failed to unblock signal in results");
}

int main(void) {
   int count = 0;
   double sum = 0;
   double x;
   struct sigaction act;

   act.sa_handler = handler; 
   act.sa_flags = 0;

   // empty the signal set and create a signal handler for SIGUSR1
   if ((sigemptyset(&act.sa_mask) == -1) || (sigaction(SIGUSR1, &act, NULL) == -1) ) {
      perror("Failed to set SIGUSR1 signal handler");
      return 1;
   }

   fprintf(stderr, "Process %d starting calculation...\n", getpid());

   for ( ; ; ) { // an infinite loop
      if ((count % 10000) == 0) // data is written into buf every 10000 iterations
         results(count, sum); 

      x = (rand() + 0.5)/(RAND_MAX + 1.0);
      sum += sin(x);
      count++;

      if (count == INT_MAX) // if SIGUSR1 is never issued
         break;
   }

   results(count, sum);

   handler(0); // handler is called directly to write out the results

   return 0;
}
