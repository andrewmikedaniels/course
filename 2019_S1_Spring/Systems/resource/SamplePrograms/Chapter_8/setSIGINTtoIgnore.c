// This program changes the action of SIGINT from SIG_DFL to SIG_IGN.

#include <signal.h>
#include <stdio.h>

struct sigaction newact, oldact;

int main(void) {
   if (sigaction(SIGINT, NULL, &oldact) == -1) // retrieve current SIGINT handler defined by system
      perror("failed to get old handler for SIGINT");
   else if (oldact.sa_handler == SIG_DFL) { // if current SIGINT handler has default action
      newact.sa_handler = SIG_IGN; // set new SIGINT handler to "ignore" action
      if (sigaction(SIGINT, &newact, &oldact) == -1) // SIG_DFL has no effect now
         perror("failed to ignore SIGINT");
   }

   printf("no way to interrupt the program via Ctrl-C for 10 seconds\n");

   sleep(10);

   if (sigaction(SIGINT, &oldact, NULL) == -1) // set SIGINT back to SIG_DFL
      perror("failed to get old handler back for SIGINT");

   for( ; ; );

   return 0;
}
