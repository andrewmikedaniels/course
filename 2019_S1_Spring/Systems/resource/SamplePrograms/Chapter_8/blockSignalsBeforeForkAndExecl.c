// This program blocks signals before calling fork() and execl().
// The child process cannot be interrupted by SIGINT (i.e. Ctrl-C), but the parent process can.

#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void) {
   pid_t child;
   sigset_t mask, oldmask;

   // parent blocks all signals before forking a child process to execute "ls"
   if ((sigfillset(&mask) == -1) ||
      (sigprocmask(SIG_SETMASK, &mask, &oldmask) == -1)) {
      perror("Failed to block the signals");
      return 1;
   }

   if ((child = fork()) == -1) {
      perror("Failed to fork child");
      return 1;
   }

   // The child process inherits the signal mask after fork() and execl().
   // i.e. "ls" command executes with all signals blocked.
   // The child process has a copy of the original signal mask saved in oldmask,
   // but an execl() command overwrites all program variables, including oldmask.
   // So the executed child process can NOT restore the original mask once execl() 
   // takes place.
   if (child == 0) { // child code
      // child process cannot be interrupted by Ctrl-C because SIGINT cannot be unblocked
      // This is because "oldmask" is overwritten by the exec command
      execl("/bin/ls", "ls", "-l", NULL);
      perror("Child failed to exec");
      return 1;
   }

   sleep(5); // parent cannot be interrupted here by Ctrl-C

   // the parent can restore the original signal mask and then waits for the child
   // the parent can be interrupted by Ctrl-C after the following sigprocmask call is executed
   if (sigprocmask(SIG_SETMASK, &oldmask, NULL) == -1){ // parent code
      perror("Parent failed to restore signal mask");
      return 1;
   }

   if (wait(NULL) == -1) { // parent can be interrupted with Ctrl-C
      perror("Parent failed to wait for child");
      return 1;
   }

   return 0;
}
