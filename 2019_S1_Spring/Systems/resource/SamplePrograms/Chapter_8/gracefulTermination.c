// This program terminates gracefully when it receives a Ctrl-C.
// Regular Ctrl-C will terminate programs abruptly and abnormally.
// Graceful termination is achieved by replacing abrupt termination by a signal handler.
// Add -lm in the command line for compilation.

#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

static volatile sig_atomic_t doneflag = 0; // a critical section noticed by the compiler

static void setdoneflag(int signo) {
   doneflag = 1;
}

int main (void) {
   struct sigaction act;
   int count = 0;
   double sum = 0;
   double x;

   act.sa_handler = setdoneflag; // set up signal handler
   act.sa_flags = 0;

   // empty the mask set and create a signal handler for SIGINT
   if ((sigemptyset(&act.sa_mask) == -1) || (sigaction(SIGINT, &act, NULL) == -1)) {
      perror("Failed to set SIGINT handler");
      return 1;
   }

   while (!doneflag) { // an infinite loop until Ctrl-C is pressed.
      x = (rand() + 0.5)/(RAND_MAX + 1.0);
      sum += sin(x);
      count++;
      // printf() is NOT async-signal safe; however, since stdout is accessed by main()
      // only and not in the signal handler, it's safe to do it this way.
      printf("Count is %d and average is %f\n", count, sum/count);
      sleep(2);
   }

   printf("Program terminating ...\n");

   if (count == 0)
      printf("No values calculated yet\n");
   else
      printf("Count is %d and average is %f\n", count, sum/count);

   printf("Good bye...\n");

   sleep(3);

   return 0;
}
