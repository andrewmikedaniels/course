#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/stat.h>

#define BLKSIZE 1024
#define READ_FLAGS O_RDONLY
#define WRITE_FLAGS (O_WRONLY | O_CREAT | O_APPEND)
#define WRITE_PERMS (S_IRUSR | S_IWUSR)
#define READ_WRITE_FLAGS (O_RDWR | O_CREAT | O_APPEND)

int CopyFile(int fromfd, int tofd);

int main(int argc, char * argv[])
{
	int bytes;
	int childpid;
	int fd, fromfd1, fromfd2, tofd;
	char tempfile1[] = "temp1.dat";
	char tempfile2[] = "temp2.dat";

	// CHECK COMMAND-LINE ARGUMENTS

	if (argc != 4)
	{
		fprintf(stderr, "Usage: %s from_file1 from_file2 to_file\n", argv[0]);
		return 1;
	}

	// OPEN FILES

        if ((tofd = open(argv[3], WRITE_FLAGS, WRITE_PERMS)) == -1)
        {
                perror("Failed to create output file");
                return 1;
        }

	int tempfd1, tempfd2;

	if ((tempfd1 = open(tempfile1, READ_WRITE_FLAGS, WRITE_PERMS)) == -1)
	{
		perror("Failed to create interim file");
		return 1;
	}

	if ((tempfd2 = open(tempfile2, READ_WRITE_FLAGS, WRITE_PERMS)) == -1)
	{
		perror("Failed to create interim file");
		return 1;
	}

	fromfd1 = open(argv[1], READ_FLAGS);
	fromfd2 = open(argv[2], READ_FLAGS);

	if (fromfd1 == fromfd2 == -1)
	{
		fprintf(stderr, "Failed to open files: %s, %s: %s\n", argv[1], argv[2], strerror(errno));
		return 1;
	}
	else if (fromfd1 == -1)
	{
		fprintf(stderr, "Failed to open file 1: %s: %s\n", argv[1], strerror(errno));
		CopyFile(fromfd2, STDERR_FILENO);

		while (close(fromfd2) == -1);
		fromfd2 = open(argv[2], READ_FLAGS);

		CopyFile(fromfd2, tofd);
		return 1;
	}
	else if (fromfd2 == -1)
	{
		fprintf(stderr, "Failed to open file 2: %s: %s\n", argv[2], strerror(errno));
		CopyFile(fromfd1, STDERR_FILENO);

                while (close(fromfd1) == -1);
                fromfd2 = open(argv[1], READ_FLAGS);

		CopyFile(fromfd1, tofd);
		return 1;
	}

	// COPY CONTENTS

	int bytes1 = CopyFile(fromfd1, tempfd1);
	int bytes2 = CopyFile(fromfd2, tempfd2);

        while (close(tempfd1) == -1);
        while (close(tempfd2) == -1);

	tempfd1 = open(tempfile1, READ_FLAGS);
	tempfd2 = open(tempfile2, READ_FLAGS);

	if (bytes1 <= 0 && bytes2 <= 0)
	{
		printf("The input files you selected are empty.\n");
		return 0;
	}

	if (bytes1 <= 0)
	{
		printf("Input file is empty: %s\n", argv[1]);
		CopyFile(tempfd2, tofd);
		return 0;
	}

	if (bytes2 <= 0)
	{
		printf("Input file is empty: %s\n", argv[2]);
		CopyFile(tempfd1, tofd);
		return 0;
	}

	printf("Reading from files: %s, %s...\n", argv[1], argv[2]);

	if (bytes1 >= bytes2)
	{
		CopyFile(tempfd1, tofd);
		CopyFile(tempfd2, tofd);
	}
	else
	{
		CopyFile(tempfd2, tofd);
		CopyFile(tempfd1, tofd);
	}

	while (close(fromfd1) == -1);
	while (close(fromfd2) == -2);
	while (close(tempfd1) == -1);
	while (close(tempfd2) == -1);
	while (close(tofd) == -1);

	remove(tempfile1);
	remove(tempfile2);

	return 0;
}

int CopyFile(int fromfd, int tofd)
{
	char * bp;
	char buf[BLKSIZE];
	int bytesread, byteswritten;
	int totalbytes = 0;
	int done = 0;

	while (!done)
	{
		while (((bytesread = read(fromfd, buf, BLKSIZE)) == -1) &&
		      (errno == EINTR));

		if (bytesread <= 0)
			break;

		bp = buf;

		while (bytesread > 0)
		{
			while (((byteswritten = write(tofd, bp, bytesread)) == -1) &&
			      (errno == EINTR));

			if (byteswritten <= 0)
				break;

			totalbytes += byteswritten;
			bytesread  -= byteswritten;

			if (bytesread == 0)
				done = 1;
		}

		if (byteswritten == -1)
			break;
	}

	return totalbytes;
}

int ValidFd(int fd)
{
	return (fd >= 0) && (fd < FD_SETSIZE);
}

int Ready(int fd1, int fd2)
{
        int maxfd;
        int nfds;
        fd_set readset;

        if (!ValidFd(fd1) || !ValidFd(fd2))
	{
		errno = EINVAL;
		return -1;
	}

	maxfd = fd1 > fd2 ? fd1 : fd2;

	FD_ZERO(&readset);
	FD_SET(fd1, &readset);
	FD_SET(fd2, &readset);

	nfds = select(maxfd + 1, &readset, NULL, NULL, NULL);

	if (nfds == -1)
		return -1;

	if (FD_ISSET(fd1, &readset))
		return fd1;

	if (FD_ISSET(fd2, &readset))
		return fd2;

	return 0;
}

int CopyFiles(int fromfd1, int tofd1, int fromfd2, int tofd2)
{
	int bytesread;
	int maxfd;
	int num;
	fd_set readset;
	int totalbytes = 0;

	if (!ValidFd(fromfd1) || !ValidFd(tofd1) || !ValidFd(fromfd2) || !ValidFd(tofd2))
		return 0;

	while (1)
	{
		num = Ready(fromfd1, fromfd2);

		if ((num == -1) && (errno == EINTR))
			continue;

		if (num == fromfd1)
		{
			bytesread = CopyFile(fromfd1, tofd1);

			if (bytesread <= 0)
				break;

			totalbytes += bytesread;
		}

		if (num == fromfd2)
		{
			bytesread = CopyFile(fromfd2, tofd2);

			if (bytesread <= 0)
				break;

			totalbytes += bytesread;
		}
	}

	return totalbytes;
}

