#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int IsPipe(char * token)
{
	int temp = 0;
	int i = 0;

	for (i = 0; token[i] != '\0' && i < 1; ++i)
	{
		temp = (int)(token[i] == '|');
	}

	return temp;
}

int main(int argc, char **argv)
{
	const char delim_1[2] = " ";
	char * tokens[100];
	char * token;

	token = strtok(argv[1], delim_1);

	int tok_num = 0;

	while (token != NULL)
	{
		tokens[tok_num++] = token;
		token = strtok(NULL, delim_1);
	}

	int what[100];
	int pipe_num = 0;
	int k = 0;
	while (k < tok_num)
	{
		if (IsPipe(tokens[k]))
			what[pipe_num++] = k;
		k = k + 1;
	}
	what[pipe_num] = -1;

	int prev = 0;
	char *a[100];
	char *b[100];
	char *c[100];
	char *d[100];
	char *e[100];
	char *f[100];

	int pipes[100];

	int z = 0;

	{
		int end;
		if (pipe_num > 0)
			end = what[0];
		else
			end = tok_num;
		for (; z < end; z++)
		{
			a[z - prev] = tokens[z];
		}
		a[z - prev] = NULL;
		pipe(pipes);
	}
	
	if (pipe_num > 0)
	{
		prev = what[0] + 1;

		int end;
		if (pipe_num > 1)
			end = what[1];
		else
			end = tok_num;
		for (; z < end; z++)
		{
			b[z - prev] = tokens[z];
		}
		b[z - prev] = NULL;
		pipe(pipes + 2);
	}

	if (pipe_num > 1)
	{
		prev = what[1] + 1;
	
		int end;
		if (pipe_num > 2)
			end = what[2];
		else
			end = tok_num;
		for (; z < end; z++)
		{
			c[z - prev] = tokens[z];
		}
		c[z - prev] = NULL;
		pipe(pipes + 4);
	}

	if (pipe_num > 2)
	{
		prev = what[2] + 1;
	
		int end;
		if (pipe_num > 3)
			end = what[3];
		else
			end = tok_num;
		for (; z < end; z++)
		{
			d[z - prev] = tokens[z];
		}
		d[z - prev] = NULL;
		pipe(pipes + 6);
	}

	if (pipe_num > 3)
	{
		prev = what[3] + 1;
	
		int end;
		if (pipe_num > 4)
			end = what[4];
		else
			end = tok_num;
		for (; z < end; z++)
		{
			d[z - prev] = tokens[z];
		}
		d[z - prev] = NULL;
		pipe(pipes + 8);
	}

	if (pipe_num > 4)
	{
		prev = what[4] + 1;
	
		int end;
		if (pipe_num > 5)
			end = what[5];
		else
			end = tok_num;
		for (; z < end; z++)
		{
			e[z - prev] = tokens[z];
		}
		e[z - prev] = NULL;
		pipe(pipes + 10);
	}

	if (pipe_num > 5)
	{
		prev = what[5] + 1;
	
		int end;
		if (pipe_num > 6)
			end = what[6];
		else
			end = tok_num;
		for (; z < end; z++)
		{
			f[z - prev] = tokens[z];
		}
		f[z - prev] = NULL;
		pipe(pipes + 12);
	}


  
  if (fork() == 0){

    if (pipe_num > 0)
      dup2(pipes[1], 1);


	int huh;
	for (huh = 0; huh < pipe_num*2; ++huh)
		close(pipes[huh]);

      execvp(*a, a);

  } else if (pipe_num > 0 && fork() == 0){
	  
	  dup2(pipes[0], 0);


	if (pipe_num > 1)
	  dup2(pipes[3], 1);


	int huh;
	for (huh = 0; huh < pipe_num*2; ++huh)
		close(pipes[huh]);

	  execvp(*b, b);

  }else if (pipe_num > 1 && fork() == 0){

	      dup2(pipes[2], 0);

	if (pipe_num > 2)
	  dup2(pipes[5], 1);


	int huh;
	for (huh = 0; huh < pipe_num*2; ++huh)
		close(pipes[huh]);

	      execvp(*c, c);

  }else if (pipe_num > 2 && fork() == 0){

	      dup2(pipes[4], 0);

	if (pipe_num > 3)
	  dup2(pipes[5], 1);


	int huh;
	for (huh = 0; huh < pipe_num*2; ++huh)
		close(pipes[huh]);

	      execvp(*d, d);

  }else if (pipe_num > 3 && fork() == 0){

	      dup2(pipes[6], 0);

	if (pipe_num > 4)
	  dup2(pipes[7], 1);


	int huh;
	for (huh = 0; huh < pipe_num*2; ++huh)
		close(pipes[huh]);

	      execvp(*e, e);

  }else if (pipe_num > 4 && fork() == 0){

	      dup2(pipes[8], 0);

	if (pipe_num > 5)
	  dup2(pipes[9], 1);


	int huh;
	for (huh = 0; huh < pipe_num*2; ++huh)
		close(pipes[huh]);

	      execvp(*f, f);
    }
      
  
	int huh;
	for (huh = 0; huh < pipe_num*2; ++huh)
		close(pipes[huh]);

  while (wait(NULL) > 0);
  //for (i = 0; i < 3; i++)
  //  wait(&status);
}
