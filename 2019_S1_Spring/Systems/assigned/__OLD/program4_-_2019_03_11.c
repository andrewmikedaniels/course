#include <signal.h>
#include <stdio.h>
#include <stdlib.h>


/***

	Program 4 Pseudocode
	
		declare global done: int
		declare global accumulator: int
		
		define handler := (signo: int): void { Output(accumulator); done <- 1 }
		
		RegisterToSignals(handler)
		done <- 0
		
		while (not done)
			BlockSignals()
			
			threshold <- Prompt()
			
			UnblockSignals()
			
			accumulator <- 0
			counter <- 1
			
			while (accumulator < threshold)
				accumulator <- accumulator + counter
				counter <- counter + 1
				
		return
		
***/


static volatile sig_atomic_t done = 0;
static int accumulator;

void EndProgram(int signo)
{
	char message[] = "Accumulator: ";
	int msgSize = sizeof(message);
	
	char output[20];
	itoa(accumulator, output, 20);
	int outputSize = sizeof(output);
	
	write(STDOUT, message, msgSize);
	write(STDOUT, output, outputSize);S
	done = 1;
}

int main()
{
	sigset_t intmask;
	
	struct sigaction act;
	
	act.sa_handler = EndProgram;
	act.sa_flags = 0;
	
	if ((sigfillset(&act.sa_mask) == -1)) {
		perror("Failed to set signal handler");
		return 1;
	}
	
	if (sigfillset(&intmask) == -1)
	{
		perror("Failed to initialize the signal mask");
		return 1;
	}
	
	while (!done)
	{
		if (sigprocmask(SIG_BLOCK, &intmask, NULL) == -1)
		{
			perror("Failed to block process mask");
			return 1;
		}
		
		int threshold;
		
		do
		{
			printf("Enter a positive integer");
			scanf("%d", &threshold);
			
			if (threshold < 0)
			{
				printf("Input must be non-negative.\n");
			}
		}
		while (threshold < 0);
		
		if (sigprocmask(SIG_UNBLOCK, &intmask, NULL) == -1)
		{
			perror("Failed to unblock process mask");
			return 1;
		}
		
		accumulator = 0;
		int counter = 1;
		
		while (accumulator < threshold)
		{
			accumulator = accumulator + counter;
			counter = counter + 1;
		}
	}
	
	return 0;
}
