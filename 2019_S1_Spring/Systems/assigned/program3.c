/*
 * main.c
 *
 *  Created on: Feb 24, 2019
 *      Author: Administrator
 */

#include <string.h>
#include <stdio.h>

int IsPipe(char * token);

int main(int argc, char * argv[])
{
	const char delim_1[3] = " ";
	const char delim_2[2] = " ";
	char * tokens[100];
	char * token;

	token = strtok(argv[1], delim_1);

	int i = 0;

	while (token != NULL)
	{
		tokens[i++] = token;
		token = strtok(NULL, delim_1);
	}

	for (int j = 0; j < i; ++j)
	{
		token = tokens[j];

		if (IsPipe(token))
		{
			// CREATE PIPE

			// FORK HERE
			int parent = 1;
			int child = 0;

			// IF CHILD
			{
				// PREPARE WRITE
				// EXECUTE
				// EXIT
			}
			// ELSE IF PARENT
			{
				// READ TO BUFFER
				// WAIT
				// CLOSE PIPE
			}

		}
	}

	return 0;
}

int IsPipe(char * token)
{
	int temp = 0;

	for (int i = 0; token[i] != '\0' && i < 1; ++i)
	{
		temp = (int)(token[i] == '|');
	}

	return temp;
}


