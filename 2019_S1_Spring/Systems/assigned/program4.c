#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


/***

	Program 4 Pseudocode
	
		declare global done: int
		declare global accumulator: int
		
		define handler := (signo: int): void { done <- 1 }
		
		Register(handler)
		done <- 0
		
		while (not done)
			BlockSignals()
			
			threshold <- Prompt()
			
			UnblockSignals()
			
			accumulator <- 0
			counter <- 1
			
			while (accumulator < threshold)
				accumulator <- accumulator + counter
				counter <- counter + 1
				
		Output(accumulator)
		return
		
***/


static volatile sig_atomic_t sig_received = 0;
static int accumulator;

void EndProgram(int signo)
{
	sig_received = 1;
}

int main()
{
	sigset_t intmask;
	
	struct sigaction act;
	
	act.sa_handler = EndProgram;
	act.sa_flags = 0;
	
	if (sigfillset(&act.sa_mask) == -1)
	{
		perror("Failed to set signal handler");
		return 1;
	}
	
	if (sigfillset(&intmask) == -1)
	{
		perror("Failed to initialize the signal mask");
		return 1;
	}

	if (sigaction(SIGINT, &act, NULL) == -1)
	{
		perror("Failed to apply sigaction handler");
	}
	
	while (!sig_received)
	{
		if (sigprocmask(SIG_BLOCK, &intmask, NULL) == -1)
		{
			perror("Failed to block process mask");
			return 1;
		}
		
		int threshold;
		char buffer[64];
		
		do
		{
			printf("Enter a positive integer: ");
			
			if (fgets(buffer, sizeof(buffer), stdin) != NULL)
			{
				threshold = -1;
				sscanf(buffer, "%d", &threshold);
				
				if (threshold < 0)
				{
					printf("Input must a be non-negative integer.\n");
				}
			}
			else
			{
				printf("Input must a be non-negative integer.\n");
			}
		}
		while (threshold < 0);
		
		if (sigprocmask(SIG_UNBLOCK, &intmask, NULL) == -1)
		{
			perror("Failed to unblock process mask");
			return 1;
		}
		
		accumulator = 0;
		int counter = 1;
		
		while (accumulator < threshold)
		{
			accumulator = accumulator + counter;
			counter = counter + 1;
		}
		
		if (!sig_received)
		{
			printf("Interrupt signal not received. Restarting...\n");
		}
	}
	
	printf("Interrupt signal received.\n");
	printf("Accumulator: %d\n", accumulator);
		
	return 0;
}
